const express = require('express');
const router = express.Router();
const teknikerRoutes = require('./routes/tekniker')
const authRoutes = require('./routes/auth')
router.use('/tekniker', teknikerRoutes)
router.use('/auth', authRoutes)

module.exports = router;