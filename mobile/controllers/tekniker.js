const conn = require('./db')
const async = require('async')

exports.get_orders = (req, res, next) => {
    var id_user = req.userData.id;

    conn.query("select a.privilege, b.id, b.id_order, b.id_customer, b.nama_pasien, c.nama as customer, b.tanggal, b.tanggal_target_selesai, b.status from tbl_spk a LEFT JOIN tbl_order b ON a.id_order = b.id LEFT JOIN tbl_customers c ON b.id_customer = c.id WHERE a.id_pegawai = " + id_user + " ORDER BY b.tanggal_target_selesai ASC", (err, rows) => {
        var items = rows;
        async.eachSeries(items, (item, cb) => {
            conn.query("select a.id_product, b.nama as nama_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id WHERE a.id_order = " + item.id + " GROUP BY a.id_product", (err, row) => {

                item.products = row;
                return res.status(200).send({ "message": "Success Get Order", "orders": item = rows })
            })
        }, error => {
            return res.status(400).send({ "message": "Cannot get data", "orders": item = rows });
        })
    })
}
exports.get_progress_per_product = (req, res, next) => {
    var id_order = req.params.id_order
    var id_product = req.params.id_product

    conn.query("select a.id, a.id_order, a.id_product, a.status, a.tanggal_selesai, b.nama as nama_proses, b.approve_spv, b.nomor, c.nama as status_text, d.nama as nama_tekniker from tbl_progress a LEFT JOIN tbl_master_proses b ON a.id_proses = b.id LEFT JOIN tbl_status_progress c ON a.status = c.id LEFT JOIN tbl_pegawai d ON a.tekniker = d.id WHERE a.id_order = " + id_order + " AND a.id_product = " + id_product + " ORDER BY b.nomor ASC", (err, rows) => {
        if (err) res.status(400).send({ "msg": err, "data": rows })
        else res.status(200).send({ "msg": err, "data": rows })
    })
}


exports.get_detail_order = (req, res, next) => {
    var id = req.params.id
    conn.query("select a.id, a.id_order, a.id_customer, a.nama_pasien, b.nama as customer, b.email, b.alamat, a.tanggal, a.tanggal_target_selesai, a.status from tbl_order a LEFT JOIN tbl_customers b ON a.id_customer = b.id WHERE a.id = " + id, (err, rows) => {
        var item = rows.length > 0 ? rows[0] : {};
        if (rows.length > 0) {
            conn.query("select a.id_product, b.nama as nama_product, a.warna, a.posisi_gigi from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id WHERE a.id_order = " + item.id, (err, row) => {
                item.details = row;
                res.json(item);
            })
        } else {
            res.json(item);
        }
    })
}


exports.get_detail_order_progress = (req, res, next) => {
    var id = req.params.id

    conn.query("select a.id, a.id_order, a.id_customer, a.nama_pasien, b.nama as customer, a.tanggal, a.tanggal_target_selesai, a.status from tbl_order a LEFT JOIN tbl_customers b ON a.id_customer = b.id WHERE a.id = " + id, (err, rows) => {
        var item = rows.length > 0 ? rows[0] : {};
        if (rows.length > 0) {
            conn.query("select a.id_product, b.nama as nama_product, (select count(id) from tbl_progress where id_order = a.id_order and id_product =  a.id_product and status = 2) as selesai, (select count(id) from tbl_progress where id_order = a.id_order and id_product = a.id_product ) as total_pekerjaan from tbl_order_detail a LEFT JOIN tbl_product b ON a.id_product = b.id WHERE a.id_order = " + item.id + " GROUP BY a.id_product", (err, row) => {
                item.products = row;
                res.json(item);
            })
        } else res.json(item);

    })
}

exports.updateProgress = function (req, res) {

    var id_order = req.body.id_order;
    var id_product = req.body.id_product;
    var id_progress = req.body.id_progress;
    var status = req.body.status;
    var tanggal_selesai = req.body.tanggal_selesai;
    var id_tekniker = req.body.id_tekniker;

    conn.query("Update tbl_progress a LEFT JOIN tbl_master_proses b ON a.id_proses = b.id LEFT JOIN tbl_status_progress c ON a.status = c.id LEFT JOIN tbl_pegawai d ON a.tekniker = d.id SET a.status = " + status + ", a.tekniker = " + id_tekniker + ", a.tanggal_selesai = " + tanggal_selesai + " WHERE a.id_proses= " + id_progress + " AND a.id_order = " + id_order + " AND a.id_product = " + id_product,
        function (error, rows) {
            if (error) res.status(400).json(error)
            else res.json(rows)
        });
};


exports.rest = (req, res, next) => {
    var item = "Server Response"
    res.send(item);
}