const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const conn = require('./db');

exports.do_login = (req, res, next) => {
    var email = req.body.email;
    var password = req.body.password;
    conn.query("select id, nama, email, password, privilege from tbl_pegawai where email = '" + email + "'", (err, rw) => {
        if (rw.length < 1) {
            return res.status(401).json({ message: 'Email & password anda tidak valid' });
        }
        var user = rw[0];
        const token = jwt.sign({
            email: email,
            nama: user.nama,
            id: user.id,
            privilege: user.privilege
        }, process.env.JWT_KEY, {});

        return res.status(200).json({ nama: user.nama, email: user.email, id: user.id, token: token });
    })
}


exports.rest = (req, res, next) => {
    var item = "Server Response"
    res.send(item);
}