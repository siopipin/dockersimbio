const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth')
const teknikerCtrl = require('../controllers/tekniker')


/* data untuk halaman/tab Works per tekniker/spv */
router.get('/orders', checkAuth, teknikerCtrl.get_orders)

/* data list pekerjaan per product & order */
router.get('/progress/:id_order/:id_product', checkAuth, teknikerCtrl.get_progress_per_product)

/* data detail order (termasuk detail item product) */
router.get('/order/:id', checkAuth, teknikerCtrl.get_detail_order)

/* data untuk halaman landing setelah scan QR Code */
router.get('/progress-order/:id', checkAuth, teknikerCtrl.get_detail_order_progress)

router.put('/update-progress/', checkAuth, teknikerCtrl.updateProgress)

router.get('/test', teknikerCtrl.rest)

module.exports = router;